package com.snakeway.pdfviewer.model;

/**
 * @author snakeway
 */
public class ClickInfo {
    private int x;
    private int y;
    private float zoom;
    private int page;
    private float scale;
    private float pdfPositionX;
    private float pdfPositionY;
    private float pdfWidth;
    private float pdfHeight;

    public ClickInfo() {
    }

    public ClickInfo(int x, int y, float zoom, int page) {
        this.x = x;
        this.y = y;
        this.zoom = zoom;
        this.page = page;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public float getZoom() {
        return zoom;
    }

    public void setZoom(float zoom) {
        this.zoom = zoom;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

    public float getPdfPositionX() {
        return pdfPositionX;
    }

    public void setPdfPositionX(float pdfPositionX) {
        this.pdfPositionX = pdfPositionX;
    }

    public float getPdfPositionY() {
        return pdfPositionY;
    }

    public void setPdfPositionY(float pdfPositionY) {
        this.pdfPositionY = pdfPositionY;
    }

    public float getPdfWidth() {
        return pdfWidth;
    }

    public void setPdfWidth(float pdfWidth) {
        this.pdfWidth = pdfWidth;
    }

    public float getPdfHeight() {
        return pdfHeight;
    }

    public void setPdfHeight(float pdfHeight) {
        this.pdfHeight = pdfHeight;
    }
}
