package com.snakeway.pdfviewer.annotation;

import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;

import com.snakeway.pdflibrary.util.Size;
import com.snakeway.pdflibrary.util.SizeF;
import com.snakeway.pdfviewer.CoordinateUtils;
import com.snakeway.pdfviewer.PDFView;
import com.snakeway.pdfviewer.annotation.base.AnnotationType;
import com.snakeway.pdfviewer.annotation.base.BaseAnnotation;
import com.snakeway.pdfviewer.annotation.pen.Pen;
import com.snakeway.pdfviewer.model.NotesRemarkInfo;

/**
 * @author snakeway
 */
public final class NotesAnnotation extends BaseAnnotation<NotesRemarkInfo, Pen.NotesPen> {
    private transient RectF areaRect;
    private transient Rect notesAreaRect;
    private transient boolean needHidden;

    public NotesAnnotation(int page, Size pageSize, Pen.NotesPen pen) {
        super(AnnotationType.NOTES, page, pageSize, pen);
    }

    @Override
    public void draw(Canvas canvas, float scale, int basePenWidth, PDFView pdfView) {
        super.draw(canvas, scale, basePenWidth, pdfView);
//        drawed=false;
    }

    public RectF getAreaRect() {
        return areaRect;
    }

    public void setAreaRect(RectF areaRect) {
        this.areaRect = areaRect;
        if (areaRect == null) {
            this.notesAreaRect = null;
        }
    }

    public Rect getNotesAreaRect() {
        return notesAreaRect;
    }

    public void setNotesAreaRect(Rect notesAreaRect) {
        this.notesAreaRect = notesAreaRect;
    }

    public boolean isNeedHidden() {
        return needHidden;
    }

    public void setNeedHidden(boolean needHidden) {
        this.needHidden = needHidden;
    }

    public RectF getPdfAreaRect(PDFView pdfView) {
        if (pdfView == null) {
            return new RectF(0, 0, 0, 0);
        }
        SizeF leftTopPdfSize = data.getLeftTopPdfSize();
        SizeF rightBottomPdfSize = data.getRightBottomPdfSize();

        Point leftTopPoint = CoordinateUtils.toPdfPointCoordinateDesc(pdfView, page, leftTopPdfSize.getWidth(), leftTopPdfSize.getHeight());
        Point rightBottomPoint = CoordinateUtils.toPdfPointCoordinateDesc(pdfView, page, rightBottomPdfSize.getWidth(), rightBottomPdfSize.getHeight());

        RectF resultRectF = new RectF(leftTopPoint.x, leftTopPoint.y, rightBottomPoint.x, rightBottomPoint.y);
        return resultRectF;
    }


    public RectF getScaleAreaRect(PDFView pdfView, RectF areaRect, float scale) {
        if (pdfView == null) {
            return new RectF(0, 0, 0, 0);
        }
        RectF resultRectF = new RectF(areaRect.left, areaRect.top, areaRect.right, areaRect.bottom);
        resultRectF.left /= scale;
        resultRectF.top /= scale;
        resultRectF.right /= scale;
        resultRectF.bottom /= scale;
        float zoom = pdfView.getZoom();
        resultRectF.left *= zoom;
        resultRectF.top *= zoom;
        resultRectF.right *= zoom;
        resultRectF.bottom *= zoom;
        return resultRectF;
    }
}
