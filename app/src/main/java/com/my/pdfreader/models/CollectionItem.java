package com.my.pdfreader.models;

public class CollectionItem {

    private String filePath;
    private String name;
    private int page;

    public CollectionItem(String filePath, String name, int page) {
        this.filePath = filePath;
        this.name = name;
        this.page = page;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
