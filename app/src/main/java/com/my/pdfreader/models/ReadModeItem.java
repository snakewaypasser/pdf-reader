package com.my.pdfreader.models;

public class ReadModeItem {
    public enum ReadModeType {
        SINGLE, MULTI, THUMBNAIL, NIGHT, AUTO_FILL_WHITE_SPACE
    }

    private ReadModeType type;

    private int icon;

    private String title;

    private boolean isCheck;

    public ReadModeItem(ReadModeType type, int icon, String title, boolean isCheck) {
        this.type = type;
        this.icon = icon;
        this.title = title;
        this.isCheck = isCheck;
    }

    public ReadModeType getType() {
        return type;
    }

    public void setType(ReadModeType type) {
        this.type = type;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

}
