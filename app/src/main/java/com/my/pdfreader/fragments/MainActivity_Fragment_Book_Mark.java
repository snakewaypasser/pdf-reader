package com.my.pdfreader.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.my.pdfreader.MainActivity;
import com.my.pdfreader.R;
import com.my.pdfreader.databinding.ActivityMainFragmentBookMarkBinding;
import com.my.pdfreader.models.BaseBookMarkBean;
import com.my.pdfreader.models.BookMarkBean;
import com.snakeway.pdflibrary.PdfDocument;

import java.util.List;

public class MainActivity_Fragment_Book_Mark extends BaseFragment<ActivityMainFragmentBookMarkBinding> {
    private View.OnClickListener onClickListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        initAll();
        return view;
    }


    @Override
    protected ActivityMainFragmentBookMarkBinding getViewBinder() {
        return ActivityMainFragmentBookMarkBinding.inflate(getLayoutInflater());
    }

    @Override
    protected void initHandler() {
        handler = new Handler();
    }

    @Override
    protected void initUi() {
        onClickListener();
    }

    @Override
    protected void initConfigUi() {
        MainActivity mainActivity = (MainActivity) getBaseActivity();
        List<PdfDocument.Bookmark> bookmarks = mainActivity.viewBinding.pdfView.getTableOfContents();
        viewBinding.treeControlView.refreshAllItem(BookMarkBean.convertBookMark(bookmarks, new BaseBookMarkBean.OnBookMarkListener() {
            @Override
            public void onItemClick(BaseBookMarkBean baseBookMarkBean) {
                mainActivity.jumpToPageWithAutoFillCheck((int) baseBookMarkBean.pageIndex);
            }
        }), true);

    }

    @Override
    protected void initHttp() {
    }

    @Override
    protected void initOther() {

    }

    private void onClickListener() {
        onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.imageViewShare:

                        break;
                    default:
                        break;
                }
            }
        };

    }

}