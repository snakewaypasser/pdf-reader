package com.my.pdfreader.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.my.pdfreader.MainActivity;
import com.my.pdfreader.adapter.CollectionRecyclerAdapter;
import com.my.pdfreader.databinding.ActivityMainFragmentCollectionBinding;
import com.my.pdfreader.models.CollectionItem;

public class MainActivity_Fragment_Collection extends BaseFragment<ActivityMainFragmentCollectionBinding> {
    CollectionRecyclerAdapter collectionRecyclerAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        initAll();
        return view;
    }


    @Override
    protected ActivityMainFragmentCollectionBinding getViewBinder() {
        return ActivityMainFragmentCollectionBinding.inflate(getLayoutInflater());
    }

    @Override
    protected void initHandler() {
        handler = new Handler();
    }

    @Override
    protected void initUi() {

    }

    @Override
    protected void initConfigUi() {
        MainActivity mainActivity = (MainActivity) getBaseActivity();
        viewBinding.recyclerView.setLayoutManager(new LinearLayoutManager(mainActivity));
        resetAdapter();
    }

    @Override
    protected void initHttp() {
    }

    @Override
    protected void initOther() {

    }


    public void resetAdapter() {
        if (!isCreate) {
            return;
        }
        MainActivity mainActivity = (MainActivity) getBaseActivity();
        collectionRecyclerAdapter = new CollectionRecyclerAdapter(mainActivity, mainActivity.getCurrentCollectionItems(mainActivity.PDF_NAME), new CollectionRecyclerAdapter.OnCollectionItemClickListener() {
            @Override
            public void onItemEdit(RecyclerView.ViewHolder viewHolder, int position, CollectionItem collectionItem) {
                mainActivity.showPopupWindowInput(viewHolder.itemView, collectionItem.getName());
            }

            @Override
            public void onItemRemove(RecyclerView.ViewHolder viewHolder, int position, CollectionItem collectionItem) {
                mainActivity.removeCollectionItem(collectionItem.getFilePath(), collectionItem.getPage());
            }

            @Override
            public void onItemClick(RecyclerView.ViewHolder viewHolder, int position, CollectionItem collectionItem) {
                mainActivity.jumpToPageWithAutoFillCheck(collectionItem.getPage());
            }
        });
        viewBinding.recyclerView.setAdapter(collectionRecyclerAdapter);
    }

    public void notifyDataSetChanged() {
        collectionRecyclerAdapter.notifyDataSetChanged();
    }


}