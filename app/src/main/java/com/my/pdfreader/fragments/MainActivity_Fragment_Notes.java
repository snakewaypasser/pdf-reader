package com.my.pdfreader.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.my.pdfreader.MainActivity;
import com.my.pdfreader.adapter.NotesRecyclerAdapter;
import com.my.pdfreader.databinding.ActivityMainFragmentNotesBinding;
import com.my.pdfreader.models.NotesItem;

public class MainActivity_Fragment_Notes extends BaseFragment<ActivityMainFragmentNotesBinding> {
    NotesRecyclerAdapter notesRecyclerAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        initAll();
        return view;
    }


    @Override
    protected ActivityMainFragmentNotesBinding getViewBinder() {
        return ActivityMainFragmentNotesBinding.inflate(getLayoutInflater());
    }

    @Override
    protected void initHandler() {
        handler = new Handler();
    }

    @Override
    protected void initUi() {

    }

    @Override
    protected void initConfigUi() {
        MainActivity mainActivity = (MainActivity) getBaseActivity();
        viewBinding.recyclerView.setLayoutManager(new LinearLayoutManager(mainActivity));
        resetAdapter();
    }

    @Override
    protected void initHttp() {
    }

    @Override
    protected void initOther() {

    }


    public void resetAdapter() {
        if (!isCreate) {
            return;
        }
        MainActivity mainActivity = (MainActivity) getBaseActivity();
        notesRecyclerAdapter = new NotesRecyclerAdapter(mainActivity, mainActivity.getCurrentNotesItems(mainActivity.PDF_NAME), new NotesRecyclerAdapter.OnNotesItemClickListener() {
            @Override
            public void onItemEdit(RecyclerView.ViewHolder viewHolder, int position, NotesItem notesItem) {
                mainActivity.showPopupWindowInputNotes(viewHolder.itemView, true, notesItem.getValue(), notesItem.getId(), null);
            }

            @Override
            public void onItemRemove(RecyclerView.ViewHolder viewHolder, int position, NotesItem notesItem) {
                mainActivity.removeNotesItem(notesItem.getFilePath(), notesItem.getId());
            }

            @Override
            public void onItemClick(RecyclerView.ViewHolder viewHolder, int position, NotesItem notesItem) {
                mainActivity.jumpToPageWithAutoFillCheck(notesItem.getPage());
            }
        });
        viewBinding.recyclerView.setAdapter(notesRecyclerAdapter);
    }

    public void notifyDataSetChanged() {
        notesRecyclerAdapter.notifyDataSetChanged();
    }

}