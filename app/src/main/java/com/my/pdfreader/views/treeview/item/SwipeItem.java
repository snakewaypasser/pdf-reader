package com.my.pdfreader.views.treeview.item;


import androidx.annotation.NonNull;

import com.my.pdfreader.views.treeview.base.ViewHolder;
import com.my.pdfreader.views.treeview.widget.swipe.SwipeItemMangerInterface;
import com.my.pdfreader.views.treeview.widget.swipe.SwipeLayout;


public interface SwipeItem {

    int getSwipeLayoutId();

    SwipeLayout.DragEdge getDragEdge();

    void onBindSwipeView(@NonNull ViewHolder viewHolder, int position, SwipeItemMangerInterface swipeManger);

    void openCallback();
}
