package com.my.pdfreader.views.treeview.widget.swipe;

public enum SwipeMode {
    Single, Multiple
}