package com.my.pdfreader.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.my.pdfreader.BaseActivity;
import com.my.pdfreader.R;
import com.my.pdfreader.models.CollectionItem;

import java.util.ArrayList;
import java.util.List;

public class CollectionRecyclerAdapter extends RecyclerView.Adapter<CollectionRecyclerAdapter.CollectionViewHolder> {
    private BaseActivity baseActivity;
    private List<CollectionItem> datas;
    private OnCollectionItemClickListener onCollectionItemClickListener;

    public CollectionRecyclerAdapter(BaseActivity baseActivity, List<CollectionItem> datas, OnCollectionItemClickListener onCollectionItemClickListener) {
        if (datas == null) {
            datas = new ArrayList<>();
        }
        this.baseActivity = baseActivity;
        this.datas = datas;
        this.onCollectionItemClickListener = onCollectionItemClickListener;
    }

    @NonNull
    @Override
    public CollectionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(baseActivity).inflate(R.layout.activity_main_fragment_collection_item, parent, false);
        return new CollectionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CollectionViewHolder viewHolder, @SuppressLint("RecyclerView") int position) {
        final CollectionItem collectionItem = datas.get(position);
        viewHolder.textView.setText(collectionItem.getName());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewHolder.linearLayoutOperating.getVisibility() == View.VISIBLE) {
                    viewHolder.linearLayoutOperating.setVisibility(View.GONE);
                    viewHolder.imageViewMore.setVisibility(View.VISIBLE);
                } else {
                    if (onCollectionItemClickListener != null) {
                        onCollectionItemClickListener.onItemClick(viewHolder, position, collectionItem);
                    }
                }
            }
        });
        viewHolder.imageViewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewHolder.linearLayoutOperating.setVisibility(View.VISIBLE);
                viewHolder.imageViewMore.setVisibility(View.GONE);
            }
        });
        viewHolder.imageViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onCollectionItemClickListener != null) {
                    onCollectionItemClickListener.onItemEdit(viewHolder, position, collectionItem);
                }
            }
        });
        viewHolder.imageViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onCollectionItemClickListener != null) {
                    onCollectionItemClickListener.onItemRemove(viewHolder, position, collectionItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    public final class CollectionViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        public LinearLayout linearLayoutOperating;
        public ImageView imageViewDelete;
        public ImageView imageViewEdit;
        public ImageView imageViewMore;

        public CollectionViewHolder(@NonNull View view) {
            super(view);
            textView = (TextView) view.findViewById(R.id.textView);
            linearLayoutOperating = (LinearLayout) view.findViewById(R.id.linearLayoutOperating);
            imageViewDelete = (ImageView) view.findViewById(R.id.imageViewDelete);
            imageViewEdit = (ImageView) view.findViewById(R.id.imageViewEdit);
            imageViewMore = (ImageView) view.findViewById(R.id.imageViewMore);
        }
    }

    public interface OnCollectionItemClickListener {
        void onItemEdit(RecyclerView.ViewHolder viewHolder, int position, CollectionItem collectionItem);

        void onItemRemove(RecyclerView.ViewHolder viewHolder, int position, CollectionItem collectionItem);

        void onItemClick(RecyclerView.ViewHolder viewHolder, int position, CollectionItem collectionItem);

    }
}
