package com.my.pdfreader.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.my.pdfreader.BaseActivity;
import com.my.pdfreader.R;
import com.my.pdfreader.models.NotesItem;

import java.util.ArrayList;
import java.util.List;

public class NotesRecyclerAdapter extends RecyclerView.Adapter<NotesRecyclerAdapter.CollectionViewHolder> {
    private BaseActivity baseActivity;
    private List<NotesItem> datas;
    private OnNotesItemClickListener onNotesItemClickListener;

    public NotesRecyclerAdapter(BaseActivity baseActivity, List<NotesItem> datas, OnNotesItemClickListener onNotesItemClickListener) {
        if (datas == null) {
            datas = new ArrayList<>();
        }
        this.baseActivity = baseActivity;
        this.datas = datas;
        this.onNotesItemClickListener = onNotesItemClickListener;
    }

    @NonNull
    @Override
    public CollectionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(baseActivity).inflate(R.layout.activity_main_fragment_notes_item, parent, false);
        return new CollectionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CollectionViewHolder viewHolder, @SuppressLint("RecyclerView") int position) {
        final NotesItem notesItem = datas.get(position);
        if (needShowPage(position)) {
            viewHolder.frameLayoutPage.setVisibility(View.VISIBLE);
            viewHolder.textViewPage.setText("第" + (notesItem.getPage() + 1) + "页");
        } else {
            viewHolder.frameLayoutPage.setVisibility(View.GONE);
        }
        viewHolder.textView.setText(notesItem.getValue());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewHolder.linearLayoutOperating.getVisibility() == View.VISIBLE) {
                    viewHolder.linearLayoutOperating.setVisibility(View.GONE);
                    viewHolder.imageViewMore.setVisibility(View.VISIBLE);
                } else {
                    if (onNotesItemClickListener != null) {
                        onNotesItemClickListener.onItemClick(viewHolder, position, notesItem);
                    }
                }
            }
        });
        viewHolder.imageViewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewHolder.linearLayoutOperating.setVisibility(View.VISIBLE);
                viewHolder.imageViewMore.setVisibility(View.GONE);
            }
        });
        viewHolder.imageViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onNotesItemClickListener != null) {
                    onNotesItemClickListener.onItemEdit(viewHolder, position, notesItem);
                }
            }
        });
        viewHolder.imageViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onNotesItemClickListener != null) {
                    onNotesItemClickListener.onItemRemove(viewHolder, position, notesItem);
                }
            }
        });
    }

    private boolean needShowPage(int position) {
        if (position < 1) {
            return true;
        }
        int oldPage = datas.get(position - 1).getPage();
        int page = datas.get(position).getPage();
        if (oldPage != page) {
            return true;
        }
        return false;
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    public final class CollectionViewHolder extends RecyclerView.ViewHolder {
        public FrameLayout frameLayoutPage;
        public TextView textViewPage;
        public TextView textView;
        public LinearLayout linearLayoutOperating;
        public ImageView imageViewDelete;
        public ImageView imageViewEdit;
        public ImageView imageViewMore;

        public CollectionViewHolder(@NonNull View view) {
            super(view);
            frameLayoutPage = (FrameLayout) view.findViewById(R.id.frameLayoutPage);
            textViewPage = (TextView) view.findViewById(R.id.textViewPage);
            textView = (TextView) view.findViewById(R.id.textView);
            linearLayoutOperating = (LinearLayout) view.findViewById(R.id.linearLayoutOperating);
            imageViewDelete = (ImageView) view.findViewById(R.id.imageViewDelete);
            imageViewEdit = (ImageView) view.findViewById(R.id.imageViewEdit);
            imageViewMore = (ImageView) view.findViewById(R.id.imageViewMore);
        }
    }

    public interface OnNotesItemClickListener {
        void onItemEdit(RecyclerView.ViewHolder viewHolder, int position, NotesItem notesItem);

        void onItemRemove(RecyclerView.ViewHolder viewHolder, int position, NotesItem notesItem);

        void onItemClick(RecyclerView.ViewHolder viewHolder, int position, NotesItem notesItem);
    }
}
